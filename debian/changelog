libdvd-pkg (1.4.3-1-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update debconf template translation
    - Swedish translation.
      Thanks to Martin Bagge (Closes: #994558)
    - Brazilian Portuguese translation.
      Thanks to Paulo Henrique de Lima Santana (Closes: #1025431)
    - Romanian translation.
      Thanks to Remus-Gabriel Chelu (Closes: #1090924)

 -- Helge Kreutzmann <debian@helgefjell.de>  Tue, 07 Jan 2025 20:28:13 +0100

libdvd-pkg (1.4.3-1-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Drop custom source compression.
  * Bump debhelper from old 9 to 12.

  [ Marcos Fouces ]
  * Add templates.pot file

  [ Sebastian Ramacher ]
  * debian/po/es.po: Add Spanish translation (Closes: 987343)
  * New upstream release (Closes: #989985)
  * libdvdcss/: Modernize packaging
  * debian/control: Bump debhelper compat to 13
  * debian/rules: Fix order of tar arguments

  [ Hideki Yamane ]
  * check with https
  * use https for the download (Closes: #865347)

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 07 Sep 2021 00:48:16 +0200

libdvd-pkg (1.4.2-1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Sebastian Ramacher ]
  * New upstream release.
  * Remove some extra newlines.
  * debian/control: Bump Standards-Version.

 -- Sebastian Ramacher <sramacher@debian.org>  Fri, 20 Apr 2018 16:35:26 +0200

libdvd-pkg (1.4.1-1-1) unstable; urgency=medium

  * Team upload.

  [ Sebastian Ramacher ]
  * New upstream release.
  * debian/control: Bump Standards-Version.

  [ Stephen Thomas ]
  * Fix invocation of dpkg-buildpackage if libcap2-bin is not available.
    (Closes: #859761)

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 18 Jan 2018 23:20:32 +0100

libdvd-pkg (1.4.0-1-2) unstable; urgency=medium

  [ Carlos Maddela ]
  * Use alternative way to check if dpkg database is locked.
    Version 1.18.7 of dpkg also returns an error status of 2 when
    attempting to install /dev/zero, so this method can no longer
    be used to check the lock status. Trying to purge a non-existent
    package achieves the original behaviour (Closes: #824093).

  [ Dmitry Smirnov ]
  * Vcs-Browser URL to HTTPS.
  * Standards-Version: 3.9.8.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 15 May 2016 20:06:43 +1000

libdvd-pkg (1.4.0-1-1) unstable; urgency=medium

  * New upstream release [December 2015].
  * Debconf templates and debian/control reviewed by the
    debian-l10n-english team as part of the Smith review project
    (Closes: #793937).
    Special thanks to Justin B Rye for his improvements to templates and
    package description.
  * Debconf translation updates:
    + Czech (Michal Simunek). (Closes: #799139).
    + French (Julien Patriarca). (Closes: #799596).
    + Italian (Beatrice Torracca). (Closes: #800510, #803672).
    + Dutch (Frans Spiesschaert). (Closes: #800511).
    + German (Chris Leick). (Closes: #799806).
    + Russian (Yuri Kozlov). (Closes: #800382).
    + Portuguese (Américo Monteiro). (Closes: #800478).
    + Danish (Joe Dalton). (Closes: #800806).
  * get-orig-source: use explicit path to system `wget` and `uscan`
    (Closes: #797113).
    Thanks, Austin English.

 -- Dmitry Smirnov <onlyjob@debian.org>  Mon, 04 Jan 2016 13:57:17 +1100

libdvd-pkg (1.3.99-1-1) unstable; urgency=low

  * New upstream release [January 2015].
  * get-orig-source: reproducible sorting.

 -- Dmitry Smirnov <onlyjob@debian.org>  Tue, 28 Jul 2015 08:50:38 +1000

libdvd-pkg (1.3.0-1-1) unstable; urgency=low

  * First release (Closes: #687624).
    Thanks to Ansgar Burchardt, Benjamin Drung, Lucas Nussbaum,
    Stefano Zacchiroli and to "debian-l10n-english" team.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 15 Apr 2015 12:19:48 +1000
