#!/bin/sh

. /usr/lib/libdvd-pkg/VARS

## check if we have a newer version of guest package to install.
## package version is mangled to append '~local' to debian revision number
dpkg --compare-versions "${VERGG}~local" gt "${VERG}"
[ $? = 0 ] || exit 0

## we have newer package to build/install

## Check if this very version of guest package was previously installed
## and avoid re-installing if it was removed.
if [ -f "${DIR}/${PKGG}-${VERGG}.is-installed" ]; then
    echo "${PKGI}: Package ${PKGG}-${VERGG} was removed, stop processing..."
    exit 0
fi

## check if DPKG database is locked
dpkg -P non-existent-package 2>/dev/null
if [ "$?" -eq 2 ]; then
    echo "${PKGI}: dpkg database is locked. You may need to use command \"sudo dpkg-reconfigure ${PKGI}\"."
    if [ -h "/etc/apt/apt.conf.d/${P88}" ]; then
        echo "${PKGI}: Building and installation of package(s) [${PKGG_ALL}] postponed till after next APT operation."
    fi
    exit 0
fi

## Download orig.tar (if needed)
if [ ! -s ${DIR}/*_${VERGG%%-*}.orig.tar.* ]; then
    echo "${PKGI}: Downloading orig source..."
    [ -d "${DIR}" ] || mkdir -p ${DIR}
    cd ${DIR} \
    && /usr/share/${PKGI}/debian/rules get-orig-source \
    || exit 1
fi

echo "${PKGI}: Checking orig.tar integrity..."
sha256sum --check --strict /usr/share/${PKGI}/*_${VERGG%%-*}.orig.tar.*.sha256
if [ $? -ne 0 ]; then
    rm -fv ${DIR}/*_${VERGG%%-*}.orig.tar.*
    echo "Source archive failed integrity check so it was deleted. Please try again."
    exit 1
fi

apt-get check >/dev/null 2>&1
if [ "$?" -ne 0 ]; then
    echo "${PKGI}: \`apt-get check\` failed, you may have broken packages. Aborting..."
    exit 0
fi

set -e

## preparing to build
echo "${PKGI}: Unpacking and configuring..."
[ -d "${DIR}/build" ] && rm -rf ${DIR}/build
mkdir ${DIR}/build
cd ${DIR}/build
tar xf ../*_${VERGG%%-*}.orig.tar.* --strip-components 1
cp -r /usr/share/${PKGI}/debian .
## append '~local' to debian revision
perl -pi -e "s{[\d.]+-\d+\K}{~local}" debian/changelog
## inject ${PKGI} to guest package Depends to uninstall generated packages on installer remove.
perl -pi -e "s{^Depends:\K}{ ${PKGI}, }mg" debian/control

## building package
echo "${PKGI}: Building the package... (it may take a while)"
BUILDLOG=/usr/src/${PKGI}/${PKGG}_${VERGG}~local_$(dpkg-architecture -qDEB_HOST_ARCH).build
echo "${PKGI}: Build log will be saved to ${BUILDLOG}"
BUILDCMD="dpkg-buildpackage -b -uc >${BUILDLOG} 2>&1"
CAPSH="$(which capsh) --secbits=0x14 --drop=cap_dac_read_search,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog-ep --print --" || CAPSH=/bin/bash
${CAPSH} -c "${BUILDCMD}"

## installing
echo "${PKGI}: Installing..."
dpkg --install /usr/src/${PKGI}/*_${VERGG}~local_*.deb \
&& touch "${DIR}/${PKGG}-${VERGG}.is-installed" \
|| echo "${PKGI}: dpkg database is probably locked. Please try \"sudo dpkg-reconfigure ${PKGI}\"."

